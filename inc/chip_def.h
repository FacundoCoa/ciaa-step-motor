
#define LPC_SCT_BASE              0x40000000
#define LPC_GPDMA_BASE            0x40002000
#define LPC_SPIFI_BASE            0x40003000
#define LPC_SDMMC_BASE            0x40004000
#define LPC_EMC_BASE              0x40005000
#define LPC_USB0_BASE             0x40006000
#define LPC_USB1_BASE             0x40007000
#define LPC_LCD_BASE              0x40008000
#define LPC_FMCA_BASE             0x4000C000
#define LPC_FMCB_BASE             0x4000D000
#define LPC_ETHERNET_BASE         0x40010000
#define LPC_ATIMER_BASE           0x40040000
#define LPC_REGFILE_BASE          0x40041000
#define LPC_PMC_BASE              0x40042000
#define LPC_CREG_BASE             0x40043000
#define LPC_EVRT_BASE             0x40044000
#define LPC_RTC_BASE              0x40046000
#define LPC_CGU_BASE              0x40050000
#define LPC_CCU1_BASE             0x40051000
#define LPC_CCU2_BASE             0x40052000
#define LPC_RGU_BASE              0x40053000
#define LPC_WWDT_BASE             0x40080000
#define LPC_USART0_BASE           0x40081000
#define LPC_USART2_BASE           0x400C1000
#define LPC_USART3_BASE           0x400C2000
#define LPC_UART1_BASE            0x40082000
#define LPC_SSP0_BASE             0x40083000
#define LPC_SSP1_BASE             0x400C5000
#define LPC_TIMER0_BASE           0x40084000
#define LPC_TIMER1_BASE           0x40085000
#define LPC_TIMER2_BASE           0x400C3000
#define LPC_TIMER3_BASE           0x400C4000
#define LPC_SCU_BASE              0x40086000
#define LPC_PIN_INT_BASE          0x40087000
#define LPC_GPIO_GROUP_INT0_BASE  0x40088000
#define LPC_GPIO_GROUP_INT1_BASE  0x40089000
#define LPC_MCPWM_BASE            0x400A0000
#define LPC_I2C0_BASE             0x400A1000
#define LPC_I2C1_BASE             0x400E0000
#define LPC_I2S0_BASE             0x400A2000
#define LPC_I2S1_BASE             0x400A3000
#define LPC_C_CAN1_BASE           0x400A4000
#define LPC_RITIMER_BASE          0x400C0000
#define LPC_QEI_BASE              0x400C6000
#define LPC_GIMA_BASE             0x400C7000
#define LPC_DAC_BASE              0x400E1000
#define LPC_C_CAN0_BASE           0x400E2000
#define LPC_ADC0_BASE             0x400E3000
#define LPC_ADC1_BASE             0x400E4000
#define LPC_ADCHS_BASE            0x400F0000
#define LPC_GPIO_PORT_BASE        0x400F4000
#define LPC_SPI_BASE              0x40100000
#define LPC_SGPIO_BASE            0x40101000
#define LPC_EEPROM_BASE           0x4000E000
#define LPC_ROM_API_BASE          0x10400100

#define LPC_SCT                   ((LPC_SCT_T              *) LPC_SCT_BASE)
#define LPC_GPDMA                 ((LPC_GPDMA_T            *) LPC_GPDMA_BASE)
#define LPC_SDMMC                 ((LPC_SDMMC_T            *) LPC_SDMMC_BASE)
#define LPC_EMC                   ((LPC_EMC_T              *) LPC_EMC_BASE)
#define LPC_USB0                  ((LPC_USBHS_T            *) LPC_USB0_BASE)
#define LPC_USB1                  ((LPC_USBHS_T            *) LPC_USB1_BASE)
#define LPC_LCD                   ((LPC_LCD_T              *) LPC_LCD_BASE)
#define LPC_ETHERNET              ((LPC_ENET_T             *) LPC_ETHERNET_BASE)
#define LPC_ATIMER                ((LPC_ATIMER_T           *) LPC_ATIMER_BASE)
#define LPC_REGFILE               ((LPC_REGFILE_T          *) LPC_REGFILE_BASE)
#define LPC_PMC                   ((LPC_PMC_T              *) LPC_PMC_BASE)
#define LPC_EVRT                  ((LPC_EVRT_T             *) LPC_EVRT_BASE)
#define LPC_RTC                   ((LPC_RTC_T              *) LPC_RTC_BASE)
#define LPC_CGU                   ((LPC_CGU_T              *) LPC_CGU_BASE)
#define LPC_CCU1                  ((LPC_CCU1_T             *) LPC_CCU1_BASE)
#define LPC_CCU2                  ((LPC_CCU2_T             *) LPC_CCU2_BASE)
#define LPC_CREG                  ((LPC_CREG_T             *) LPC_CREG_BASE)
#define LPC_RGU                   ((LPC_RGU_T              *) LPC_RGU_BASE)
#define LPC_WWDT                  ((LPC_WWDT_T             *) LPC_WWDT_BASE)
#define LPC_USART0                ((LPC_USART_T            *) LPC_USART0_BASE)
#define LPC_USART2                ((LPC_USART_T            *) LPC_USART2_BASE)
#define LPC_USART3                ((LPC_USART_T            *) LPC_USART3_BASE)
#define LPC_UART1                 ((LPC_USART_T            *) LPC_UART1_BASE)
#define LPC_SSP0                  ((LPC_SSP_T              *) LPC_SSP0_BASE)
#define LPC_SSP1                  ((LPC_SSP_T              *) LPC_SSP1_BASE)
#define LPC_TIMER0                ((LPC_TIMER_T            *) LPC_TIMER0_BASE)
#define LPC_TIMER1                ((LPC_TIMER_T            *) LPC_TIMER1_BASE)
#define LPC_TIMER2                ((LPC_TIMER_T            *) LPC_TIMER2_BASE)
#define LPC_TIMER3                ((LPC_TIMER_T            *) LPC_TIMER3_BASE)
#define LPC_SCU                   ((LPC_SCU_T              *) LPC_SCU_BASE)
#define LPC_GPIO_PIN_INT          ((LPC_PIN_INT_T          *) LPC_PIN_INT_BASE)
#define LPC_GPIOGROUP             ((LPC_GPIOGROUPINT_T     *) LPC_GPIO_GROUP_INT0_BASE)
#define LPC_MCPWM                 ((LPC_MCPWM_T            *) LPC_MCPWM_BASE)
#define LPC_I2C0                  ((LPC_I2C_T              *) LPC_I2C0_BASE)
#define LPC_I2C1                  ((LPC_I2C_T              *) LPC_I2C1_BASE)
#define LPC_I2S0                  ((LPC_I2S_T              *) LPC_I2S0_BASE)
#define LPC_I2S1                  ((LPC_I2S_T              *) LPC_I2S1_BASE)
#define LPC_C_CAN1                ((LPC_CCAN_T             *) LPC_C_CAN1_BASE)
#define LPC_RITIMER               ((LPC_RITIMER_T          *) LPC_RITIMER_BASE)
#define LPC_QEI                   ((LPC_QEI_T              *) LPC_QEI_BASE)
#define LPC_GIMA                  ((LPC_GIMA_T             *) LPC_GIMA_BASE)
#define LPC_DAC                   ((LPC_DAC_T              *) LPC_DAC_BASE)
#define LPC_C_CAN0                ((LPC_CCAN_T             *) LPC_C_CAN0_BASE)
#define LPC_ADC0                  ((LPC_ADC_T              *) LPC_ADC0_BASE)
#define LPC_ADC1                  ((LPC_ADC_T              *) LPC_ADC1_BASE)
#define LPC_ADCHS                 ((LPC_HSADC_T            *) LPC_ADCHS_BASE)
#define LPC_GPIO_PORT             ((LPC_GPIO_T             *) LPC_GPIO_PORT_BASE)
#define LPC_SPI                   ((LPC_SPI_T              *) LPC_SPI_BASE)
#define LPC_SGPIO                 ((LPC_SGPIO_T            *) LPC_SGPIO_BASE)
#define LPC_EEPROM                ((LPC_EEPROM_T           *) LPC_EEPROM_BASE)
#define LPC_FMCA                  ((LPC_FMC_T              *) LPC_FMCA_BASE)
#define LPC_FMC                   ((LPC_FMC_T            * *) LPC_FMCA_BASE)
#define LPC_FMCB                  ((LPC_FMC_T              *) LPC_FMCB_BASE)
#define LPC_ROM_API               ((LPC_ROM_API_T          *) LPC_ROM_API_BASE)


/* Crystal frequency into device for RTC/32K input */
#define CRYSTAL_32K_FREQ_IN 32768

#define 	CGU_IRC_FREQ   (12000000)


/* System configuration variables used by chip driver */
const unsigned int ExtRateIn = 0;
const unsigned int OscRateIn = 12000000;


/**
 * @brief CGU clock input list
 * These are possible input clocks for the CGU and can come
 * from both external (crystal) and internal (PLL) sources. These
 * clock inputs can be routed to the base clocks (@ref CHIP_CGU_BASE_CLK_T).
 */
typedef enum CHIP_CGU_CLKIN {
	CLKIN_32K,		/*!< External 32KHz input */
	CLKIN_IRC,		/*!< Internal IRC (12MHz) input */
	CLKIN_ENET_RX,	/*!< External ENET_RX pin input */
	CLKIN_ENET_TX,	/*!< External ENET_TX pin input */
	CLKIN_CLKIN,	/*!< External GPCLKIN pin input */
	CLKIN_RESERVED1,
	CLKIN_CRYSTAL,	/*!< External (main) crystal pin input */
	CLKIN_USBPLL,	/*!< Internal USB PLL input */
	CLKIN_AUDIOPLL,	/*!< Internal Audio PLL input */
	CLKIN_MAINPLL,	/*!< Internal Main PLL input */
	CLKIN_RESERVED2,
	CLKIN_RESERVED3,
	CLKIN_IDIVA,	/*!< Internal divider A input */
	CLKIN_IDIVB,	/*!< Internal divider B input */
	CLKIN_IDIVC,	/*!< Internal divider C input */
	CLKIN_IDIVD,	/*!< Internal divider D input */
	CLKIN_IDIVE,	/*!< Internal divider E input */
	CLKINPUT_PD		/*!< External 32KHz input */
} CHIP_CGU_CLKIN_T;

/**
 * @brief CGU base clocks
 * CGU base clocks are clocks that are associated with a single input clock
 * and are routed out to 1 or more peripherals. For example, the CLK_BASE_PERIPH
 * clock can be configured to use the CLKIN_MAINPLL input clock, which will in
 * turn route that clock to the CLK_PERIPH_BUS, CLK_PERIPH_CORE, and
 * CLK_PERIPH_SGPIO periphral clocks.
 */
typedef enum CHIP_CGU_BASE_CLK {
	CLK_BASE_SAFE,		/*!< Base clock for WDT oscillator, IRC input only */
	CLK_BASE_USB0,		/*!< Base USB clock for USB0, USB PLL input only */
	CLK_BASE_PERIPH,	/*!< Base clock for SGPIO */
	CLK_BASE_USB1,		/*!< Base USB clock for USB1 */
	CLK_BASE_MX,		/*!< Base clock for CPU core */
	CLK_BASE_SPIFI,		/*!< Base clock for SPIFI */
	CLK_BASE_SPI,		/*!< Base clock for SPI */
	CLK_BASE_PHY_RX,	/*!< Base clock for PHY RX */
	CLK_BASE_PHY_TX,	/*!< Base clock for PHY TX */
	CLK_BASE_APB1,		/*!< Base clock for APB1 group */
	CLK_BASE_APB3,		/*!< Base clock for APB3 group */
	CLK_BASE_LCD,		/*!< Base clock for LCD pixel clock */
	CLK_BASE_ADCHS,		/*!< Base clock for ADCHS */
	CLK_BASE_SDIO,		/*!< Base clock for SDIO */
	CLK_BASE_SSP0,		/*!< Base clock for SSP0 */
	CLK_BASE_SSP1,		/*!< Base clock for SSP1 */
	CLK_BASE_UART0,		/*!< Base clock for UART0 */
	CLK_BASE_UART1,		/*!< Base clock for UART1 */
	CLK_BASE_UART2,		/*!< Base clock for UART2 */
	CLK_BASE_UART3,		/*!< Base clock for UART3 */
	CLK_BASE_OUT,		/*!< Base clock for CLKOUT pin */
	CLK_BASE_RESERVED4,
	CLK_BASE_RESERVED5,
	CLK_BASE_RESERVED6,
	CLK_BASE_RESERVED7,
	CLK_BASE_APLL,		/*!< Base clock for audio PLL */
	CLK_BASE_CGU_OUT0,	/*!< Base clock for CGUOUT0 pin */
	CLK_BASE_CGU_OUT1,	/*!< Base clock for CGUOUT1 pin */
	CLK_BASE_LAST,
	CLK_BASE_NONE = CLK_BASE_LAST
} CHIP_CGU_BASE_CLK_T;

/**
 * @brief CGU dividers
 * CGU dividers provide an extra clock state where a specific clock can be
 * divided before being routed to a peripheral group. A divider accepts an
 * input clock and then divides it. To use the divided clock for a base clock
 * group, use the divider as the input clock for the base clock (for example,
 * use CLKIN_IDIVB, where CLKIN_MAINPLL might be the input into the divider).
 */
typedef enum CHIP_CGU_IDIV {
	CLK_IDIV_A,		/*!< CGU clock divider A */
	CLK_IDIV_B,		/*!< CGU clock divider B */
	CLK_IDIV_C,		/*!< CGU clock divider A */
	CLK_IDIV_D,		/*!< CGU clock divider D */
	CLK_IDIV_E,		/*!< CGU clock divider E */
	CLK_IDIV_LAST
} CHIP_CGU_IDIV_T;

#define CHIP_CGU_IDIV_MASK(x)  ("\x03\x0F\x0F\x0F\xFF"[x])

/**
 * @brief Peripheral clocks
 * Peripheral clocks are individual clocks routed to peripherals. Although
 * multiple peripherals may share a same base clock, each peripheral's clock
 * can be enabled or disabled individually. Some peripheral clocks also have
 * additional dividers associated with them.
 */
typedef enum CHIP_CCU_CLK {
	/* CCU1 clocks */
	CLK_APB3_BUS,		/*!< APB3 bus clock from base clock CLK_BASE_APB3 */
	CLK_APB3_I2C1,		/*!< I2C1 register/perigheral clock from base clock CLK_BASE_APB3 */
	CLK_APB3_DAC,		/*!< DAC peripheral clock from base clock CLK_BASE_APB3 */
	CLK_APB3_ADC0,		/*!< ADC0 register/perigheral clock from base clock CLK_BASE_APB3 */
	CLK_APB3_ADC1,		/*!< ADC1 register/perigheral clock from base clock CLK_BASE_APB3 */
	CLK_APB3_CAN0,		/*!< CAN0 register/perigheral clock from base clock CLK_BASE_APB3 */
	CLK_APB1_BUS = 32,	/*!< APB1 bus clock clock from base clock CLK_BASE_APB1 */
	CLK_APB1_MOTOCON,	/*!< Motor controller register/perigheral clock from base clock CLK_BASE_APB1 */
	CLK_APB1_I2C0,		/*!< I2C0 register/perigheral clock from base clock CLK_BASE_APB1 */
	CLK_APB1_I2S,		/*!< I2S register/perigheral clock from base clock CLK_BASE_APB1 */
	CLK_APB1_CAN1,		/*!< CAN1 register/perigheral clock from base clock CLK_BASE_APB1 */
	CLK_SPIFI = 64,		/*!< SPIFI SCKI input clock from base clock CLK_BASE_SPIFI */
	CLK_MX_BUS = 96,	/*!< M3/M4 BUS core clock from base clock CLK_BASE_MX */
	CLK_MX_SPIFI,		/*!< SPIFI register clock from base clock CLK_BASE_MX */
	CLK_MX_GPIO,		/*!< GPIO register clock from base clock CLK_BASE_MX */
	CLK_MX_LCD,			/*!< LCD register clock from base clock CLK_BASE_MX */
	CLK_MX_ETHERNET,	/*!< ETHERNET register clock from base clock CLK_BASE_MX */
	CLK_MX_USB0,		/*!< USB0 register clock from base clock CLK_BASE_MX */
	CLK_MX_EMC,			/*!< EMC clock from base clock CLK_BASE_MX */
	CLK_MX_SDIO,		/*!< SDIO register clock from base clock CLK_BASE_MX */
	CLK_MX_DMA,			/*!< DMA register clock from base clock CLK_BASE_MX */
	CLK_MX_MXCORE,		/*!< M3/M4 CPU core clock from base clock CLK_BASE_MX */
	RESERVED_ALIGN = CLK_MX_MXCORE + 3,
	CLK_MX_SCT,			/*!< SCT register clock from base clock CLK_BASE_MX */
	CLK_MX_USB1,		/*!< USB1 register clock from base clock CLK_BASE_MX */
	CLK_MX_EMC_DIV,		/*!< ENC divider clock from base clock CLK_BASE_MX */
	CLK_MX_FLASHA,		/*!< FLASHA bank clock from base clock CLK_BASE_MX */
	CLK_MX_FLASHB,		/*!< FLASHB bank clock from base clock CLK_BASE_MX */
	CLK_M4_M0APP,		/*!< M0 app CPU core clock from base clock CLK_BASE_MX */
	CLK_MX_ADCHS,		/*!< ADCHS clock from base clock CLK_BASE_ADCHS */
	CLK_MX_EEPROM,		/*!< EEPROM clock from base clock CLK_BASE_MX */
	CLK_MX_WWDT = 128,	/*!< WWDT register clock from base clock CLK_BASE_MX */
	CLK_MX_UART0,		/*!< UART0 register clock from base clock CLK_BASE_MX */
	CLK_MX_UART1,		/*!< UART1 register clock from base clock CLK_BASE_MX */
	CLK_MX_SSP0,		/*!< SSP0 register clock from base clock CLK_BASE_MX */
	CLK_MX_TIMER0,		/*!< TIMER0 register/perigheral clock from base clock CLK_BASE_MX */
	CLK_MX_TIMER1,		/*!< TIMER1 register/perigheral clock from base clock CLK_BASE_MX */
	CLK_MX_SCU,			/*!< SCU register/perigheral clock from base clock CLK_BASE_MX */
	CLK_MX_CREG,		/*!< CREG clock from base clock CLK_BASE_MX */
	CLK_MX_RITIMER = 160,	/*!< RITIMER register/perigheral clock from base clock CLK_BASE_MX */
	CLK_MX_UART2,		/*!< UART3 register clock from base clock CLK_BASE_MX */
	CLK_MX_UART3,		/*!< UART4 register clock from base clock CLK_BASE_MX */
	CLK_MX_TIMER2,		/*!< TIMER2 register/perigheral clock from base clock CLK_BASE_MX */
	CLK_MX_TIMER3,		/*!< TIMER3 register/perigheral clock from base clock CLK_BASE_MX */
	CLK_MX_SSP1,		/*!< SSP1 register clock from base clock CLK_BASE_MX */
	CLK_MX_QEI,			/*!< QEI register/perigheral clock from base clock CLK_BASE_MX */
	CLK_PERIPH_BUS = 192,	/*!< Peripheral bus clock from base clock CLK_BASE_PERIPH */
	CLK_RESERVED3,
	CLK_PERIPH_CORE,	/*!< Peripheral core clock from base clock CLK_BASE_PERIPH */
	CLK_PERIPH_SGPIO,	/*!< SGPIO clock from base clock CLK_BASE_PERIPH */
	CLK_USB0 = 224,			/*!< USB0 clock from base clock CLK_BASE_USB0 */
	CLK_USB1 = 256,			/*!< USB1 clock from base clock CLK_BASE_USB1 */
	CLK_SPI = 288,			/*!< SPI clock from base clock CLK_BASE_SPI */
	CLK_ADCHS = 320,		/*!< ADCHS clock from base clock CLK_BASE_ADCHS */
	CLK_CCU1_LAST,

	/* CCU2 clocks */
	CLK_CCU2_START,
	CLK_APLL = CLK_CCU2_START,	/*!< Audio PLL clock from base clock CLK_BASE_APLL */
	RESERVED_ALIGNB = CLK_CCU2_START + 31,
	CLK_APB2_UART3,			/*!< UART3 clock from base clock CLK_BASE_UART3 */
	RESERVED_ALIGNC = CLK_CCU2_START + 63,
	CLK_APB2_UART2,			/*!< UART2 clock from base clock CLK_BASE_UART2 */
	RESERVED_ALIGND = CLK_CCU2_START + 95,
	CLK_APB0_UART1,			/*!< UART1 clock from base clock CLK_BASE_UART1 */
	RESERVED_ALIGNE = CLK_CCU2_START + 127,
	CLK_APB0_UART0,			/*!< UART0 clock from base clock CLK_BASE_UART0 */
	RESERVED_ALIGNF = CLK_CCU2_START + 159,
	CLK_APB2_SSP1,			/*!< SSP1 clock from base clock CLK_BASE_SSP1 */
	RESERVED_ALIGNG = CLK_CCU2_START + 191,
	CLK_APB0_SSP0,			/*!< SSP0 clock from base clock CLK_BASE_SSP0 */
	RESERVED_ALIGNH = CLK_CCU2_START + 223,
	CLK_APB2_SDIO,			/*!< SDIO clock from base clock CLK_BASE_SDIO */
	CLK_CCU2_LAST
} CHIP_CCU_CLK_T;

/**
 * @}
 */

/**
 * @brief CREG Register Block
 */
typedef struct {						/*!< CREG Structure         */
	unsigned int  RESERVED0;
	unsigned int  CREG0;				/*!< Chip configuration register 32 kHz oscillator output and BOD control register. */
	unsigned int  RESERVED1[62];
	unsigned int  MXMEMMAP;			/*!< ARM Cortex-M3/M4 memory mapping */
    unsigned int  RESERVED2;
    unsigned int  CREG1;				/*!< Configuration Register 1 */
    unsigned int  CREG2;				/*!< Configuration Register 2 */
    unsigned int  CREG3;				/*!< Configuration Register 3 */
    unsigned int  CREG4;				/*!< Configuration Register 4 */
	unsigned int  CREG5;				/*!< Chip configuration register 5. Controls JTAG access. */
	unsigned int  DMAMUX;				/*!< DMA muxing control     */
	unsigned int  FLASHCFGA;			/*!< Flash accelerator configuration register for flash bank A */
	unsigned int  FLASHCFGB;			/*!< Flash accelerator configuration register for flash bank B */
	unsigned int  ETBCFG;				/*!< ETB RAM configuration  */
	unsigned int  CREG6;				/*!< Chip configuration register 6. */
	unsigned int  M4TXEVENT;			/*!< M4 IPC event register */
	unsigned int  RESERVED4[51];
	unsigned int  CHIPID;				/*!< Part ID                */
	unsigned int  RESERVED5[65];
	unsigned int  M0SUBMEMMAP;         /*!< M0SUB IPC Event memory mapping */
	unsigned int  RESERVED6[2];
	unsigned int  M0SUBTXEVENT;        /*!< M0SUB IPC Event register */
	unsigned int  RESERVED7[58];
	unsigned int  M0APPTXEVENT;		/*!< M0APP IPC Event register */
	unsigned int  M0APPMEMMAP;			/*!< ARM Cortex M0APP memory mapping */
	unsigned int  RESERVED8[62];
	unsigned int  USB0FLADJ;			/*!< USB0 frame length adjust register */
	unsigned int  RESERVED9[63];
	unsigned int  USB1FLADJ;			/*!< USB1 frame length adjust register */
} LPC_CREG_T;




/**
 * Audio or USB PLL selection
 */
typedef enum CHIP_CGU_USB_AUDIO_PLL {
	CGU_USB_PLL,
	CGU_AUDIO_PLL
} CHIP_CGU_USB_AUDIO_PLL_T;

/**
 * PLL register block
 */
typedef struct {
	unsigned int  PLL_STAT;				/*!< PLL status register */
	unsigned int  PLL_CTRL;				/*!< PLL control register */
	unsigned int  PLL_MDIV;				/*!< PLL M-divider register */
	unsigned int  PLL_NP_DIV;				/*!< PLL N/P-divider register */
} CGU_PLL_REG_T;

/**
 * @brief LPC18XX/43XX CGU register block structure
 */
typedef struct {							/*!< (@ 0x40050000) CGU Structure          */
	unsigned int  RESERVED0[5];
	unsigned int  FREQ_MON;				/*!< (@ 0x40050014) Frequency monitor register */
	unsigned int  XTAL_OSC_CTRL;			/*!< (@ 0x40050018) Crystal oscillator control register */
	CGU_PLL_REG_T  PLL[CGU_AUDIO_PLL + 1];	/*!< (@ 0x4005001C) USB and audio PLL blocks */
	unsigned int  PLL0AUDIO_FRAC;			/*!< (@ 0x4005003C) PLL0 (audio)           */
	unsigned int  PLL1_STAT;				/*!< (@ 0x40050040) PLL1 status register   */
	unsigned int  PLL1_CTRL;				/*!< (@ 0x40050044) PLL1 control register  */
	unsigned int  IDIV_CTRL[CLK_IDIV_LAST];/*!< (@ 0x40050048) Integer divider A-E control registers */
	unsigned int  BASE_CLK[CLK_BASE_LAST];	/*!< (@ 0x4005005C) Start of base clock registers */
} LPC_CGU_T;

/**
 * @brief CCU clock config/status register pair
 */
typedef struct {
	unsigned int  CFG;						/*!< CCU clock configuration register */
	unsigned int  STAT;					/*!< CCU clock status register */
} CCU_CFGSTAT_T;

/**
 * @brief CCU1 register block structure
 */
typedef struct {							/*!< (@ 0x40051000) CCU1 Structure         */
	unsigned int  PM;						/*!< (@ 0x40051000) CCU1 power mode register */
	unsigned int  BASE_STAT;				/*!< (@ 0x40051004) CCU1 base clocks status register */
	unsigned int  RESERVED0[62];
	CCU_CFGSTAT_T  CLKCCU[CLK_CCU1_LAST];	/*!< (@ 0x40051100) Start of CCU1 clock registers */
} LPC_CCU1_T;

/**
 * @brief CCU2 register block structure
 */
typedef struct {							/*!< (@ 0x40052000) CCU2 Structure         */
	unsigned int  PM;						/*!< (@ 0x40052000) Power mode register    */
	unsigned int  BASE_STAT;				/*!< (@ 0x40052004) CCU base clocks status register */
	unsigned int  RESERVED0[62];
	CCU_CFGSTAT_T  CLKCCU[CLK_CCU2_LAST - CLK_CCU1_LAST];	/*!< (@ 0x40052100) Start of CCU2 clock registers */
} LPC_CCU2_T;







/*
*
* Definiciones de Reloj
*
*
*
*/



/* Internal oscillator frequency */
#define CGU_IRC_FREQ (12000000)

#ifndef MAX_CLOCK_FREQ
#define MAX_CLOCK_FREQ (204000000)
#endif

#define PLL_MIN_CCO_FREQ 156000000  /**< Min CCO frequency of main PLL */
#define PLL_MAX_CCO_FREQ 320000000  /**< Max CCO frequency of main PLL */

/**
 * @brief	PLL Parameter strucutre
 */
typedef struct {
	int ctrl;       /**< Control register value */
	CHIP_CGU_CLKIN_T srcin; /**< Input clock Source see #CHIP_CGU_CLKIN_T */
	int nsel;       /**< Pre-Div value */
	int psel;       /**< Post-Div Value */
	int msel;       /**< M-Div value */
	unsigned int fin;   /**< Input frequency */
	unsigned int fout;  /**< Output frequency */
	unsigned int fcco;  /**< CCO frequency */
} PLL_PARAM_T;
