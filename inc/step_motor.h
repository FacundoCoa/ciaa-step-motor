// GPIO Register (General Purpose Input/Output)
typedef struct {				// Estructura para GPIO
	unsigned char B[128][32];	// Offset 0x0000: Byte pin registers ports 0 to n; pins PIOn_0 to PIOn_31 */
	int W[32][32];				// Offset 0x1000: Word pin registers port 0 to n
	int DIR[32];				// Offset 0x2000: Direction registers port n
	int MASK[32];				// Offset 0x2080: Mask register port n
	int PIN[32];				// Offset 0x2100: Portpin register port n
	int MPIN[32];				// Offset 0x2180: Masked port register port n
	int SET[32];				// Offset 0x2200: Write: Set register for port n Read: output bits for port n
	int CLR[32];				// Offset 0x2280: Clear port n
	int NOT[32];				// Offset 0x2300: Toggle port n
} GPIO_T;

// SCU Register (System Control Unit)
typedef struct {
	int  SFSP[16][32];		// Los pines digitales estan divididos en 16 grupos (P0-P9 y PA-PF)
	int  RESERVED0[256];
	int  SFSCLK[4];			// Pin configuration register for pins CLK0-3
	int  RESERVED16[28];
	int  SFSUSB;			// Pin configuration register for USB
	int  SFSI2C0;			// Pin configuration register for I2C0-bus pins
	int  ENAIO[3];			// Analog function select registers
	int  RESERVED17[27];
	int  EMCDELAYCLK;		// EMC clock delay register
	int  RESERVED18[63];
	int  PINTSEL[2];		// Pin interrupt select register for pin int 0 to 3 index 0, 4 to 7 index 1
} SCU_T;

#define GPIO_PORT_BASE		0x400F4000
#define SCU_BASE			0x40086000

#define GPIO_PORT			((GPIO_T *) GPIO_PORT_BASE)
#define SCU					((SCU_T *) SCU_BASE)


// Funcion y modo (pag. 413)
#define SCU_MODE_EPD			(0x0 << 3)		// habilita la resistencia de pull-down (deshabilita con 0)
#define SCU_MODE_EPUN			(0x0 << 4)		// habilita la resistencia de pull-up (deshabilita con 1)
#define SCU_MODE_DES			(0x2 << 3)		// deshabilita las resistencias de pull-down y pull-up
#define SCU_MODE_EHZ			(0x1 << 5)		// 1 Rapido (ruido medio con alta velocidad)
												// 0 Lento (ruido bajo con velocidad media)
#define SCU_MODE_EZI			(0x1 << 6)		// habilita buffer de entrada (deshabilita con 0)
#define SCU_MODE_ZIF_DIS		(0x1 << 7)		// deshabilita el filtro anti glitch de entrada (habilita con 1)

#define SCU_MODE_FUNC0			0x0				// seleccion de la funcion 0 del pin
#define SCU_MODE_FUNC1			0x1				// seleccion de la funcion 1 del pin
#define SCU_MODE_FUNC2			0x2				// seleccion de la funcion 2 del pin
#define SCU_MODE_FUNC3			0x3				// seleccion de la funcion 3 del pin
#define SCU_MODE_FUNC4			0x4				// seleccion de la funcion 4 del pin
#define SCU_MODE_FUNC5			0x5				// seleccion de la funcion 5 del pin

#define MD_PUP						(0x0 << 3)		/** Enable pull-up resistor at pad */
#define MD_BUK						(0x1 << 3)		/** Enable pull-down and pull-up resistor at resistor at pad (repeater mode) */
#define MD_PLN						(0x2 << 3)		/** Disable pull-down and pull-up resistor at resistor at pad */
#define MD_PDN						(0x3 << 3)		/** Enable pull-down resistor at pad */
#define MD_EHS						(0x1 << 5)		/** Enable fast slew rate */
#define MD_EZI						(0x1 << 6)		/** Input buffer enable */
#define MD_ZI						(0x1 << 7)		/** Disable input glitch filter */
#define MD_EHD0						(0x1 << 8)		/** EHD driver strength low bit */
#define MD_EHD1						(0x1 << 8)		/** EHD driver strength high bit */
#define MD_PLN_FAST					(MD_PLN | MD_EZI | MD_ZI | MD_EHS)
#define I2C0_STANDARD_FAST_MODE		(1 << 3 | 1 << 11)	/** Pin configuration for STANDARD/FAST mode I2C */
#define I2C0_FAST_MODE_PLUS			(2 << 1 | 1 << 3 | 1 << 7 | 1 << 10 | 1 << 11)	/** Pin configuration for Fast-mode Plus I2C */
#define FUNC0						0x0				/** Pin function 0 */
#define FUNC1						0x1				/** Pin function 1 */
#define FUNC2						0x2				/** Pin function 2 */
#define FUNC3						0x3				/** Pin function 3 */
#define FUNC4						0x4				/** Pin function 4 */
#define FUNC5						0x5				/** Pin function 5 */
#define FUNC6						0x6				/** Pin function 6 */
#define FUNC7						0x7				/** Pin function 7 */

#define PORT_OFFSET					0x80			/** Port offset definition */
#define PIN_OFFSET					0x04			/** Pin offset definition */

char direccion1 = 'f';
char paso1 = '8';
int  factor1a = 1;
int  factor1b = 1;
char finB1 = 'f';
char finF1 = 'f';

char direccion2 = 'f';
char paso2 = '8';
int  factor2a = 1;
int  factor2b = 1;
char finB2 = 'f';
char finF2 = 'f';

void controlMotor(char motor, char dir){
	int _t, k, pasos;
	
	
		//Compara que motor mover  MS1 y MS2 son pines del EasyDriver 
		switch(motor){
			
			case '1':
			
				if(dir == 'f'){
					
					//Setear el pin de DIR (HIGH) del EasyDriver para que la direccion sea hacia adelante.
					GPIO_PORT->SET[3] |= 1 << 7;
					direccion1 = 1;	
					
				}else if(dir == 'b'){

					//Clear el pin de DIR (LOW) del EasyDriver para que la direccion sea hacia adelante.
					GPIO_PORT->CLR[3] |= 1 << 7;
					direccion1 = 0;
					
				}else if(dir == '1'){
					
					//Codigo para que el motor1 tenga paso completo;
					//Setear MS1 a 0
					GPIO_PORT->CLR[5] |= 1 << 15;
					
					//Setear MS2 a 0
					GPIO_PORT->CLR[3] |= 1 << 5;
					
				}else if(dir == '2'){
					
					//Codigo para que el motor1 tenga medio paso 1/2;
					//Setear MS1 a 1
					GPIO_PORT->SET[5] |= 1 << 15;
					
					//Setear MS2 a 0
					GPIO_PORT->CLR[3] |= 1 << 5;
					
				}else if(dir == '4'){
					
					//codigo para que el motor tenga un cuarto de paso 1/4;
					//Clear MS1 a 0
					GPIO_PORT->CLR[5] |= 1 << 15;
					
					//Setear MS2 a 1
					GPIO_PORT->SET[3] |= 1 << 5;
					
				}else if(dir == '8'){
					
					//Codigo para que el motor tenga octavo de paso 1/8 (default);
					//Setear MS1 a 1
					GPIO_PORT->SET[5] |= 1 << 15;
					
					//Setear MS2 a 1
					GPIO_PORT->SET[3] |= 1 << 5;
					
				}else if(dir == 'c'){
					
					//Revisar el fin de carrera BACK antes de mover
					if(GPIO_PORT->B[2][3] == 0){
						finB1 = 't';
					}else{
						finB1 = 'f';
					}
					
					//Revisar el fin de carrera FRONT antes de mover
					if(GPIO_PORT->B[2][2] == 0){
						finF1 = 't';
					}else{
						finF1 = 'f';						
					}
					
					while(((finB1 == 'f') && (direccion1 == 0)) || ((finF1 == 'f') && direccion1 == 1)){
						
						//Mandar HIGH al pin STEP del EasyDriver;
						GPIO_PORT->SET[3] |= 1 << 3;
					
						for(_t=0; _t < 10000000/70; _t++); // Delay antes de mandar LOW
					
						//Mandar LOW al pin STEP del EasyDriver;
						GPIO_PORT->CLR[3] |= 1 << 3 ;
						
						//Revisar el fin de carrera BACK
						if(GPIO_PORT->B[2][3] == 0){
							finB1 = 't';
						}
						//Revisar el fin de carrera FRONT5
						if(GPIO_PORT->B[2][2] == 0){
							finF1 = 't';
						}
						
					}
						
				}else if(dir == 'v'){
					pasos = factor1a * factor1b;
					
					for(k=0; k<pasos; k++){
						
						//Revisar el fin de carrera BACK
						if(GPIO_PORT->B[2][3] == 0){
							finB1 = 't';
							if(direccion1 == 0){
								break;
							}
						}
						//Revisar el fin de carrera FRONT
						if(GPIO_PORT->B[2][2] == 0){
							finF1 = 't';
							if(direccion1 == 1){
								break;
							}
						}
						
						//Mandar HIGH al pin STEP del EasyDriver;
						GPIO_PORT->SET[3] |= 1 << 3;
					
						for(_t=0; _t < 10000000/70; _t++); // Delay antes de mandar LOW
					
						//Mandar LOW al pin STEP del EasyDriver;
						GPIO_PORT->CLR[3] |= 1 << 3 ;
						
					}
				break; // Case '1'
			
			case '2':
			
				if(dir == 'f'){
					
					//Setear el pin de DIR (HIGH) del EasyDriver para que la direccion sea hacia adelante.
					GPIO_PORT->SET[2] |= 1 << 8;
					direccion2 = 1;
					
				}else if(dir == 'b'){
					
					//Clear el pin de DIR (LOW) del EasyDriver para que la direccion sea hacia adelante.
					GPIO_PORT->CLR[2] |= 1 << 8;
					direccion2 = 0;
					
				}else if(dir == '1'){
					
					//codigo para que el motor tenga paso completo 1;
					//Setear MS1 a 0
					GPIO_PORT->CLR[5] |= 1 << 16;
					
					//Setear MS2 a 0
					GPIO_PORT->CLR[3] |= 1 << 6;
					
				}else if(dir == '2'){
					
					//Codigo para que el motor tenga medio paso 1/2;
					//Setear MS1 a 1
					GPIO_PORT->SET[5] |= 1 << 16;
					
					//Setear MS2 a 0
					GPIO_PORT->CLR[3] |= 1 << 6;
					
				}else if(dir == '4'){
					
					//codigo para que el motor tenga un cuarto de paso 1/4;
					//Setear MS1 a 0
					GPIO_PORT->CLR[5] |= 1 << 16;
					
					//Setear MS2 a 1
					GPIO_PORT->SET[3] |= 1 << 6;
					
				}else if(dir == '8'){
					
					//Codigo para que el motor tenga octavo de paso 1/8 (default);
					//Setear MS1 a 1
					GPIO_PORT->SET[5] |= 1 << 16;
					
					//Setear MS2 a 1
					GPIO_PORT->SET[3] |= 1 << 6;
					
				}else if(dir == 'c'){
					
					//Revisar el fin de carrera BACK antes de mover
					if(GPIO_PORT->B[2][0] == 0){
						finB2 = 't';
					}else{
						finB2 = 'f';
					}
					
					//Revisar el fin de carrera FRONT antes de mover
					if(GPIO_PORT->B[3][12] == 0){
						finF2 = 't';
					}else{
						finF2 = 'f';
					}
					
					while(((finB2 == 'f') && (direccion2 == 0)) || ((finF2 == 'f') && direccion2 == 1)){
						//Mandar HIGH al pin STEP del EasyDriver;
						GPIO_PORT->SET[3] |= 1 << 4;
						for(_t=0; _t < 10000000/70; _t++); // Delay antes de mandar LOW
						
						//Mandar LOW al pin STEP del EasyDriver;
						GPIO_PORT->CLR[3] |= 1 << 4;
						
						
						//Revisar el fin de carrera BACK
						if(GPIO_PORT->B[2][0] == 0){
							finB2 = 't';
						}else{
							finB2 = 'f';
						}
					
						//Revisar el fin de carrera FRONT
						if(GPIO_PORT->B[3][12] == 0){
							finF2 = 't';
						}else{
							finF2 = 'f';
						}
							
					}
					
				}else if(dir == 'v'){
					pasos = factor2a * factor2b;
					
					for(k=0; k<pasos; k++){
						//Revisar el fin de carrera BACK antes de mover
						if(GPIO_PORT->B[2][0] == 0){
							finB2 = 't';
							if( direccion2 == 0){
								break;
							}
						}
						//Revisar el fin de carrera FRONT antes de mover
						if(GPIO_PORT->B[3][12] == 0){
							finF2 = 't';
							if( direccion2 == 1){
								break;
							}
						}
						
						//Mandar HIGH al pin STEP del EasyDriver;
						GPIO_PORT->SET[3] |= 1 << 4;
						for(_t=0; _t < 10000000/70; _t++); // Delay antes de mandar LOW
						
						//Mandar LOW al pin STEP del EasyDriver;
						GPIO_PORT->CLR[3] |= 1 << 4;
					}
				}


				
				
			break;
			
			default:
			break;
		}
		}
	
}

void configPines(){
	
	//Configuracion de los pines STEP del EasyDriver.
	
	// Habilita el PIN GPIO1 de la edu ciaa (STEP de motor1)
	SCU->SFSP[6][1] = (MD_PDN | FUNC0); //Func0 para GPIO y MD_PDN para activar la resistencia pull down y tener normalmente LOW.
	GPIO_PORT->DIR[3] |= 1 << 3; // Modo salida - Output
	GPIO_PORT->CLR[3] |= 1 << 3; // ponemos LOW a la salida.
	
	// Habilita el PIN GPIO2 de la edu ciaa (STEP de motor2)
	SCU->SFSP[6][5] = (MD_PDN | FUNC0); //Func0 para GPIO y MD_PDN para activar la resistencia pull down y tener normalmente LOW.
	GPIO_PORT->DIR[3] |= 1 << 4; // Modo salida - Output
	GPIO_PORT->CLR[3] |= 1 << 4; // ponemos LOW a la salida.
	
	
	
	//Configuracion de los pines DIR del EasyDriver
	
	// Habilita el PIN GPIO7 de la edu ciaa (DIR de motor1)
	SCU->SFSP[6][11] = (MD_PDN | FUNC0); //Func0 para GPIO y MD_PDN para activar la resistencia pull down y tener normalmente LOW.
	GPIO_PORT->DIR[3] |= 1 << 7; // Modo salida - Output
	GPIO_PORT->CLR[3] |= 1 << 7; // ponemos LOW a la salida.
	
	// Habilita el PIN GPIO8 de la edu ciaa (DIR de motor2)
	SCU->SFSP[6][12] = (MD_PDN | FUNC0); //Func0 para GPIO y MD_PDN para activar la resistencia pull down y tener normalmente LOW.
	GPIO_PORT->DIR[2] |= 1 << 8; // Modo salida - Output
	GPIO_PORT->CLR[2] |= 1 << 8; // ponemos LOW a la salida.
	
	
	
	//Configuracion de los pines MS1 del EasyDriver
	
	// Habilita el PIN GPIO3 de la edu ciaa (MS1 de motor1)
	SCU->SFSP[6][7] = (MD_PUP | FUNC4); //Func4 para GPIO y MD_PUP para activar la resistencia pull up y tener normalmente HIGH.
	GPIO_PORT->DIR[5] |= 1 << 15; // Modo salida - Output
	GPIO_PORT->SET[5] |= 1 << 15; // ponemos HIGH a la salida.
	
	// Habilita el PIN GPIO4 de la edu ciaa (MS1 de motor2)
	SCU->SFSP[6][8] = (MD_PUP | FUNC4); //Func4 para GPIO y MD_PUP para activar la resistencia pull up y tener normalmente HIGH.
	GPIO_PORT->DIR[5] |= 1 << 16; // Modo salida - Output
	GPIO_PORT->SET[5] |= 1 << 16; // ponemos HIGH a la salida.
	
	
	
	//Configuracion de los pines MS2 del EasyDriver
	
	// Habilita el PIN GPIO5 de la edu ciaa (MS2 de motor1)
	SCU->SFSP[6][9] = (MD_PUP | FUNC0); //Func0 para GPIO y MD_PUP para activar la resistencia pull up y tener normalmente HIGH.
	GPIO_PORT->DIR[3] |= 1 << 5; // Modo salida - Output
	GPIO_PORT->SET[3] |= 1 << 5; // ponemos HIGH a la salida.
	
	// Habilita el PIN GPIO6 de la edu ciaa (MS2 de motor2)
	SCU->SFSP[6][10] = (MD_PUP | FUNC0); //Func0 para GPIO y MD_PUP para activar la resistencia pull up y tener normalmente HIGH.
	GPIO_PORT->DIR[3] |= 1 << 6; // Modo salida - Output
	GPIO_PORT->SET[3] |= 1 << 6; // ponemos HIGH a la salida.
	
	
	
	//Configuracion de los pines de deteccion de Fin de Carrera
	//Se configuran con PullUP para que esten normalmente altos. Cuando se pongan a tierra deshabilitaran las opciones 'f' y  'b' de cada motor
	
	// Habilita el PIN T_FIL2 de la edu ciaa (Fin de carrera Frente motor 1, 'f')
	SCU->SFSP[4][2] = (MD_PUP|MD_EZI|MD_ZI|FUNC0); //Func0 para GPIO y MD_PUP para activar la resistencia pull up y tener normalmente HIGH.
	GPIO_PORT->DIR[2] &= ~(1 << 2); // Modo Entrada - Input (poner 0 en el registro DIR)

	// Habilita el PIN T_FIL3 de la edu ciaa (Fin de carrera Back motor 1, 'b')
	SCU->SFSP[4][3] = (MD_PUP|MD_EZI|MD_ZI|FUNC0); //Func0 para GPIO y MD_PUP para activar la resistencia pull up y tener normalmente HIGH.
	GPIO_PORT->DIR[2] &= ~(1 << 3); // Modo salida - Output
	

	// Habilita el PIN T_COL1 de la edu ciaa (Fin de carrera Frente Motor 1)
	SCU->SFSP[7][4] = (MD_PUP|MD_EZI|MD_ZI| FUNC0); //Func40 para GPIO y MD_PUP para activar la resistencia pull up y tener normalmente HIGH.
	GPIO_PORT->DIR[3] &= ~(1 << 12); // Modo Entrada - Input

	
	// Habilita el PIN T_FIL0 de la edu ciaa (Fin de carrera Back motor 2)
	SCU->SFSP[4][0] = (MD_PUP|MD_EZI|MD_ZI | FUNC0); //Func0 para GPIO y MD_PUP para activar la resistencia pull up y tener normalmente HIGH. 
	GPIO_PORT->DIR[2] &= ~(1 << 0); // Modo Entrada - Input (poner 0 en el registro DIR)
	
	
	
	
}
