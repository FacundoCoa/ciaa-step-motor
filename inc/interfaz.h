
void printMenu(char menu){
	
	
	char saludo[12] = { 'i', 'n', 'i', 'c', 'i', 'o','>', '\0'};
						 
	char motor1[12] = { 'm', 'o', 't', 'o', 'r', '1', '>', '\0'};
	char conf1[12] = { 'c', 'o', 'n', 'f', '.', '1', '>', '\0'};
	
	
	char motor2[12] = { 'm', 'o', 't', 'o', 'r', '2', '>', '\0'};
	char conf2[12] = { 'c', 'o', 'n', 'f', '.', '2', '>', '\0'};

	int i, t;
	i=0;

	 switch(menu) {
		case '1': 
			while(motor1[i] != '\0'){
				LPC_USART2->THR = (unsigned int) motor1[i];
				for(t=0; t < 10000000/50; t++);
				i++;
			}
			i=0;
			flush(3);
			break;
		 
		case '2': 
			while(motor2[i] != '\0'){
				LPC_USART2->THR = (unsigned int) motor2[i];
				for(t=0; t < 10000000/50; t++);
				i++;
			}
			i=0;
			flush(3);
			break;
		case '3':
			while(conf1[i] != '\0'){
				LPC_USART2->THR = (unsigned int) conf1[i];
				for(t=0; t < 10000000/50; t++);
				i++;
			}
			i=0;
			flush(3);
			break;
		case '4':
			while(conf2[i] != '\0'){
				LPC_USART2->THR = (unsigned int) conf2[i];
				for(t=0; t < 10000000/50; t++);
				i++;
			}
			i=0;
			flush(3);
			break;
			
		default:
			while(saludo[i] != '\0'){
				LPC_USART2->THR = (unsigned int) saludo[i];
				for(t=0; t < 10000000/50; t++);
				i++;
			}
			i=0;
			flush(3);
			break;
	 }
	
}

void IConfigMotor1(){
	enviarChar('\n');
	enviarChar('\r');
	printMenu('3');
	char ch = '\0';
	int t;
	do{
		ch = UART_LeerChar(LPC_USART2);
		for(t=0; t < 10000000/60; t++);
		if(ch == '\r'){			
			enviarChar('\n');
		}
		enviarChar(ch);
		switch(ch){
			case '\r':
				printMenu('3');
				ch = '\0';
				break;

			case '1':
				factor1a = 1;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '2':
				factor1a = 2;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '3':
				factor1a = 3;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '4':
				factor1a = 4;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '5':
				factor1a = 5;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '6':
				factor1a = 6;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '7':
				factor1a = 7;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '8':
				factor1a = 8;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '9':
				factor1a = 9;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '0':
				factor1a = 10;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case 'u':
				factor1b = 1;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case 'd':
				factor1b = 10;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case 'c':
				factor1b = 100;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case 'm':
				factor1b = 1000;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case 'x':
				enviarChar('\n');
				enviarChar('\r');
				printMenu('1');	
				break;
		}
	}while(ch != 'x');
}

void IConfigMotor2(){
	enviarChar('\n');
	enviarChar('\r');
	printMenu('4');
	char ch = '\0';
	int t;
	do{
		ch = UART_LeerChar(LPC_USART2);
		for(t=0; t < 10000000/60; t++);
		if(ch == '\r'){			
			enviarChar('\n');
		}
		enviarChar(ch);
		switch(ch){
			case '\r':
				printMenu('4');
				ch = '\0';
				break;

			case '1':
				factor2a = 1;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '2':
				factor2a = 2;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '3':
				factor2a = 3;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '4':
				factor2a = 4;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '5':
				factor2a = 5;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '6':
				factor2a = 6;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '7':
				factor2a = 7;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '8':
				factor2a = 8;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '9':
				factor2a = 9;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case '0':
				factor2a = 10;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case 'u':
				factor2b = 1;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case 'd':
				factor2b = 10;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case 'c':
				factor2b = 100;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case 'm':
				factor2b = 1000;
				//Enviar caracter para notificar el cambio.
				enviarChar('/');
				break;
			case 'x':
				enviarChar('\n');
				enviarChar('\r');
				printMenu('2');	
				break;
		}
	}while(ch != 'x');
}

void maq_motor1(){
	char ch = '\0';
	int t;
	enviarChar('\n');
	enviarChar('\r');
	printMenu('1');
	do{
		
		ch = UART_LeerChar(LPC_USART2);
		for(t=0; t < 10000000/60; t++);
		if(ch == '\r'){			
			enviarChar('\n');
		}
		enviarChar(ch);
		switch(ch){
			case '\r':
				printMenu('1');
				ch = '\0';
				break;

			case '1':
				controlMotor('1', ch);
				//Enviar caracter para notificar el cambio.
				enviarChar('!');
				break;
				
			case '2':
				controlMotor('1', ch);
				//Enviar caracter para notificar el cambio.
				enviarChar('!');
				break;
				
			case '4':
				controlMotor('1', ch);
				//Enviar caracter para notificar el cambio.
				enviarChar('!');
				break;
				
			case '8':;
				controlMotor('1', ch);
				//Enviar caracter para notificar el cambio.
				enviarChar('!');
				break;

			case 'f':
				controlMotor('1', ch);
				//Enviar caracter para notificar el cambio.
				enviarChar('>');
				break;
				
			case 'b':
				controlMotor('1', ch);
				//Enviar caracter para notificar el cambio.				
				enviarChar('<');
				break;
			
			case 's':
				IConfigMotor1(ch);
				break;
			case 'c':
				controlMotor('1', ch);
				//Enviar caracter para notificar Fin de carrera.				
				enviarChar('*');
				break;
			case 'v':
				controlMotor('1', ch);
				//Enviar caracter para notificar el paso.				
				enviarChar('+');
				break;
			case 'x':
				//Caracter de salida de maquina de motor 1;
				enviarChar('\n');
				enviarChar('\r');
				printMenu('d');	
				break;
			default:
				
				break;
			
		}
		
		
	}while(ch != 'x');
}
void maq_motor2(){
	char ch = '\0';
	int t;
	enviarChar('\n');
	enviarChar('\r');
	printMenu('2');
	do{
		
		ch = UART_LeerChar(LPC_USART2);
		for(t=0; t < 10000000/60; t++);
		if(ch == '\r'){			
			enviarChar('\n');
		}
		enviarChar(ch);
		switch(ch){
			case '\r':
				printMenu('2');
				ch = '\0';
				break;
			case '1':
				controlMotor('2', ch);
				//Enviar caracter para notificar el cambio.
				enviarChar('!');
				break;
			case '2':
				controlMotor('2', ch);
				//Enviar caracter para notificar el cambio.
				enviarChar('!');
				break;
			case '4':
				controlMotor('2', ch);
				//Enviar caracter para notificar el cambio.
				enviarChar('!');
				break;
			case '8':
				controlMotor('2', ch);
				//Enviar caracter para notificar el cambio.
				enviarChar('!');
				break;
			case 'f':
				controlMotor('2', ch);
				//Enviar caracter para notificar el cambio.
				enviarChar('>');
				break;
			case 'b':
				controlMotor('2', ch);
				//Enviar caracter para notificar el cambio.
				enviarChar('<');
				break;
			case 'c':
				controlMotor('2', ch);
				//Enviar caracter para notificar Fin de carrera.				
				enviarChar('*');
				break;
			case 'v':
				controlMotor('2', ch);
				//Enviar caracter para notificar el paso.				
				enviarChar('+');
				break;
			case 's':
				IConfigMotor2(ch);
				break;
			case 'x':
				//Caracter de salida de maquina de motor 1;
				enviarChar('\n');
				enviarChar('\r');
				printMenu('d');
				break;
			default:
				
				break;
			
		}
		
		
	}while(ch != 'x');
	
	
}
