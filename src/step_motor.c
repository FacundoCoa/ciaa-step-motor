/**********************************************************************************************
 * Control de dos motores de pasos en la placa EDU-CIAA, sin la utilizacion de librerias
 * Solo se trabaja con acceso directo a los registros de configuracion
 * 
 * Materia: Electronica Digital II - 2016 (UNSAM) -
 * Trabajo: Proyecto Final de la Materia - 2018 (UNSAM)
 * Fecha de creacion: 30/10/2018
 *
 * Creado Por: Facundo Coacci, Javier Penizzotto.
 * Documentacion:
 *    - UM10503 (LPC43xx ARM Cortex-M4/M0 multi-core microcontroller User Manual)
 *    - PINES UTILIZADOS DEL NXP LPC4337 JBD144 (Ing. Eric Pernia)
 **********************************************************************************************/


#include "driver_uart.h"
#include "step_motor.h"
#include "interfaz.h"

void print();

int t;


int main(void) {

	// Configuracion del pin (LED1) como GPIO, con pull-up
	// (Registro de configuracion, pag 405 / Tabla 191)
	// ADDRESS(SCU_BASE, SFSP2_10) = (SCU_MODE_EPUN | SCU_MODE_FUNC0);
	SCU->SFSP[2][10] = (SCU_MODE_DES | SCU_MODE_FUNC0); // P2_10, GPIO0[14], LED1
	
	configPines();
	
	SCU->SFSP[2][11] = (SCU_MODE_DES | SCU_MODE_FUNC0); // P2_11, GPIO1[11], LED2
	
	SCU->SFSP[2][12] = (SCU_MODE_DES | SCU_MODE_FUNC0); // P2_12, GPIO1[12], LED3
	
	SCU->SFSP[7][1] = (MD_PDN | FUNC6); // P2_11, GPIO1[11], LED2
	SCU->SFSP[7][2] = (MD_PLN | MD_EZI | MD_ZI | FUNC6); // P2_11, GPIO1[11], LED2
	
	// Configuracion del pin (LED1) como salida (Registro de direccion, pag 455 -> Tabla 261)
	// ADDRESS(GPIO_BASE, GPIO_PORT0_DIR_OFFSET) |= (1 << 14);
	GPIO_PORT->DIR[0] |= 1 << 14;
	
	GPIO_PORT->DIR[1] |= 1 << 11;

	// Blanqueo del pin (LED1) (pag 456 -> Tabla 266)
	// ADDRESS(GPIO_BASE, GPIO_PORT0_CLR_OFFSET) |= (1 << 14);
	//GPIO_PORT->CLR[0] |= 1 << 14;
	
	// Encendido del led (LED1) (pag 456 -> Tabla 265)
	// ADDRESS(GPIO_BASE, GPIO_PORT0_SET_OFFSET) |= (1 << 14);
	GPIO_PORT->SET[0] |= 1 << 14;
	GPIO_PORT->SET[1] |= 1 << 11;
	//GPIO_PORT->CLR[1] |= 1 << 11;
	
	// Sin utilizacion de la macro (para prender un led)
	// ptr = (int *)0x400F6200;	// 0x400F4000 + 0x2200 = 0x400F6200
	// *ptr |= (1 << 14); 
	
	UART_Iniciar(LPC_USART2);
	UART_CofigBaudios(LPC_USART2, 115200);
	
	//Reconfigurar UART2 8 bit de datos 1 bit stop, habilitar fifo y habilitar la transimision TX
	LPC_USART2->LCR = (UART_LCR_WLEN8 | UART_LCR_SBS_1BIT);
	LPC_USART2->FCR = (UART_FCR_FIFO_EN | UART_FCR_TRG_LEV2);
	LPC_USART2->TER2 = UART_TER2_TXEN;
	

	char h;
	
	printMenu('d');
		
	while (1) {

		
		h = UART_LeerChar(LPC_USART2);
		for(t=0; t < 10000000/20; t++);
		if(h=='\r'){
			enviarChar('\n');
			enviarChar(h);
			printMenu('d');

		} else if(h=='1'){
			enviarChar(h);
			maq_motor1();
		}else if(h == '2'){
			enviarChar(h);
			maq_motor2();
		}else{
			enviarChar(h);
		}
		
	}

	return 0;
}