Para que funcione el compilador hay que hacer algunos pasos y cambiar algunas lineas.

Instalar: GNU ARM Toolchain
Instalar: openOCD
Agregar al PATH estas dos herramientas. (Hay que agregar las rutas de las carpetas que tienen "arm-none-eabi-gcc.exe" y "openocd.exe")

Editar el archivo "~\ld\ciaa_lpc4337.ld" :

Cambiar las lineas:

INCLUDE "C:\Users\Facundo Coacci\Documents\Facultad\Electronica Digital II\Campus 2016\Encender_Led\Repo\ciaa-step-motor\ld\ciaa_lpc4337_lib.ld"
INCLUDE "C:\Users\Facundo Coacci\Documents\Facultad\Electronica Digital II\Campus 2016\Encender_Led\Repo\ciaa-step-motor\ld\ciaa_lpc4337_mem.ld"

Por las que correspondan (depende de donde clonaste el repo)

IMPORTANTE!!!!: No pushear en git el archivo modificado ciaa_lpc4337.ld con una GUI es mas facil evitarla.

